FROM openjdk:8-jdk-alpine
ADD target/holidaycheck-spring-boot-0.0.1-SNAPSHOT.jar holidaycheck-spring-boot-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","holidaycheck-spring-boot-0.0.1-SNAPSHOT.jar"]