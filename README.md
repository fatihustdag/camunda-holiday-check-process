//Docker image build 
docker build -f Dockerfile -t docker-spring .

//Docker Run Image
sudo docker run -d --name easy-camunda -p 80:8080 docker-spring:latest

//Test Api for days greater 10
localhost:8080/order/11

//Test Api for days lesser than 10
localhost:8080/order/9


//Check logs
docker logs easy-camunda


