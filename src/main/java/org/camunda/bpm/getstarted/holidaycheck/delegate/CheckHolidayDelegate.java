package org.camunda.bpm.getstarted.holidaycheck.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.getstarted.holidaycheck.constants.ProcessConstants;

import java.util.logging.Logger;

public class CheckHolidayDelegate implements JavaDelegate {
    private static final Logger LOGGER = Logger.getLogger(CheckHolidayDelegate.class.getName());


    @Override
    public void execute(DelegateExecution execution) {

        LOGGER.info("CheckHoliday Permission For Day : " + execution.getVariable(ProcessConstants.VAR_REQUESTED_NUMBER_OF_DAY));

    }

}