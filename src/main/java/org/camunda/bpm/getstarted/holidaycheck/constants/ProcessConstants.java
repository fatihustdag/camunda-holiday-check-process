package org.camunda.bpm.getstarted.holidaycheck.constants;

public class ProcessConstants {

    public static final String PROCESS_KEY_HOLIDAY = "employeeHoliday";

    public static final Integer NUMBER_OF_DAYS_ALLOWED = 10;

    public static final String VAR_REQUESTED_NUMBER_OF_DAY ="requestedDayOfHoliday";


}