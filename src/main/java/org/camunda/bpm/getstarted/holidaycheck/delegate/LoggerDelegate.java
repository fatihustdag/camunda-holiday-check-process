package org.camunda.bpm.getstarted.holidaycheck.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

public class LoggerDelegate implements JavaDelegate {

    private static final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());
    @Override
    public void execute(DelegateExecution execution){
        LOGGER.info("Rejected Holiday Permission for : "+ execution.toString());
    }
}
