package org.camunda.bpm.getstarted.holidaycheck.rest;


import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.getstarted.holidaycheck.constants.ProcessConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order/")
public class CheckHolidayResource {

        @Autowired

        private ProcessEngine camunda;

        @RequestMapping(method= RequestMethod.POST,value="{numberOfDays}")
        public void checkHoliday(@PathVariable int numberOfDays) {
            camunda.getRuntimeService().startProcessInstanceByKey(//
                    ProcessConstants.PROCESS_KEY_HOLIDAY, //
                    Variables
                            .putValue(ProcessConstants.VAR_REQUESTED_NUMBER_OF_DAY, numberOfDays));
        }




}
